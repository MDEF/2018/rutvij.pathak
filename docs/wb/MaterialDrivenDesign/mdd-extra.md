#Material Driven Design



## Historically Traditional Techniques of uses Mycelium / Mushrooms.

###Uses of Mycelium

The uses of the historical use of mycelium on its own are rare and could not be found there are many example of mushrooms being used in functions the most interesting thing that i found was at the waist of man found frozen in ice . Oldest known use of Mushroom The Iceman's fungi - [https://www.researchgate.net/publication/222280519_The_Iceman's_fungi](https://www.researchgate.net/publication/222280519_The_Iceman's_fungi)

> The discovery of a Neolithic corpse in September 1991 in an alpine glacier at the Hauslabjoch, 92 m south of the Austrian}Italian border (Neubauer, 1995), attracted wide-spread attention. The real age of the Iceman (we prefer this name, one of more than 500 names for this unique End since autumn 1991 (Ortner, 1995)) ranges according to nine independent radiocarbon measurements between 3350 and3100 BC (Prinoth-Fornwagner & Niklaus, 1995). Among the numerous items of equipment which were found with the mummied and frozen body there were three fungal objects.Two of these objects were first mentioned by the archaeologists Lippert & Spindler (1991) as polypore like structures the size of walnuts, each mounted separately on a leather strap. Referring to other parts of the Iceman's equipment they thought that these fungal objects might represent a kind of tinder used for making fire.

(Source)(PDF) The Iceman's fungi.  Available from: [https://www.researchgate.net/publication/222280519_The_Iceman's_fungi](https://www.researchgate.net/publication/222280519_The_Iceman's_fungi) [accessed Apr 30 2019].

![](ancient-mushroom3.png)

*This is the fungal body which
was found on the Iceman's Body*

###Palm Fibers

**Palm Fibers** has been used  in traditional crafts for centuries. weaving baskets and weaving garments which will stand for heavy use.

![](woven-closed-basket-botswana2.png)

*This is woven basket made in Botswana.*
It uses the flat structure of the fibers to bend it into a basket.

## Mycelium and Palm Fibers being used industrially

###Mycelium

**Mycofoam** -   Mycelium being used to make foam for packaging materials . This reduces the environmental impact of the the materials. and makes the material environmentally friendly.

These tiles are made by mixing mycelium spores and woodchips and various plant matter which contains large amounts of lignin which makes them hard to decompose.

Mycelium grows inside the mold which is put inside and takes the shape of the mold .

To make the mycelium usable , is compressed and heated to set in its shape .This kills the mushrooms and set the shape of the mycelium substrate.

[](https://www.notion.so/7af53ff0b9ae4083ac9ad396601d385e#0f464d2abd664b25850a1fd111d08cf5)


**Mushroom Leather**  -w  Mycelium is being treated in a specific way to give the  appearance of a leather making it more sustainable process in the long term .

![](leathersample.jpg)

*This is a leather sample from Mycoworks*

[**MycoFlex™](https://ecovativedesign.com/mycoflex)**  -  Ecovative Design technology is used to create Textiles , Footwear , Technical Wear and Foams used in Foam.

Ecovative Design also creates insulation blocks for walls along with tiles for the wall .

![](myceliumtile.png)

*This is the tile made of the mycelium which has been my using combining substrates and growing mycelium in a mold*

Ecovative Design has a material named "MycoFlex". Mycelium makes great foams for shoes as it is stronger that traditional urethane foams. It has a higher melting temperature than plastic. Better at insulation than plastic. It is as resilient as plastic i.e. it rebounds as much as standard plastic. Due to the property of foam it is inherently moisture and vapor permeable.


![](ecovative-shoe2.png)

*This is one of the renderings of the shoe where the Ecovative's Mycoflex is used as a lower for the shoe.*


###Palm Fibers

Following is the industrial characterizations of the fibers which will be extracted from the fibers. It has been characterized according to lignin content in the roots and the leaves.

![](extra-fiberdistribution.png)


*This is the general classification of the plant*

The paper classifies the tree based on the density of the plant. The density of the fibers decrease as the height of the plant increases .

![](extra-fiberstructure.png)

*This is the structure of the bio-fiber.*

1. Palm fibers are used as a natural absorbent for cleaning oil spills.
["Palm fibers and modified palm fibers adsorbents for different oils"](https://www.sciencedirect.com/science/article/pii/S1110016817300819)
> The cleanup of oil spill using natural adsorbents is considered as an eco-friendly and cost-effective way, emphasizing the importance of such natural and effective promising technique. Palm fibers, PFs, were used as natural sorbent material for oil spill removal.

2. Palm fibers are mostly used in the industry for making brushes for regular cleaning.
