# Material Driven Design

## Phenomenological Design
Material Driven Design started with  introduction to phenomenological design and the scientific method . The next lecture was atop the Valldaura labs . Where we had to carve spoons out of blocks of wood . This exercise was quite illuminating as something as carving a spoon could reflect the process of making . This is something I observed that I could feel the changing density of wood as I was carving this is the phenomenon I observed .
![](mdd-02.jpg)
_The process of cutting the spoon. with the knife ._

![](mdd-01.jpg)
_The almost finished spoon at the end of the carving session ._


The act of changing forces in a seemingly homogeneous looking material , to get the same texture was something I observed and felt .

##Start of Material Exploration

This was course was to be about explorations in material . My initial choice was leaves but discussions on the topic led to the conclusion that leaves aren’t really the materials to be worked with .
![](mdd-03.jpg)
_Leaves collected in Parc de la Ciutadella_

At the end of the discussion I did choose to work with mycelium and palm fibres . This something I wanted to work with them to make substrates which could be used to make some sort of packaging material for my final project .

## Mycelium : -  
![](oster-mycelium.jpg)
[Oyster Mushrooms - Resarch Link  ](http://www.makery.info/en/2015/10/12/le-bricole-it-yourself-du-champignon/)

The mycelium which was used to grow was Oyster Mushrooms (biological name to be added ) . This is mycelium on the bed of this media . The recipe is as follows by [Fifa](https://mdef.gitlab.io/fifa.jonsdottir/reflections/materialdesign/).


###Recipe for growth medium and Sterilization protocol


1. Malt agar (This recipe is enough for 16 petri plates)

- Malt extract 15 g

- Agar 10 g

- Demineralized water 500 ml
 pH 5,5

- Sterilize all tools before starting, make sure you are working within a sterile environment.

Measure powder and scrape into glass bottle, pour water into bottle and swirl around until everything is mixed together. Place glass jar into microwave and heat with 30-60 second intervals until everything is dissolved. Heat up to 85°c and then let cool. When cooled down to 55°c you can pour the plates. Work within a sterile environment while pouring the plates. Place lids on without closing the petri dish so you let the steam evaporate before closing and placing in the fridge. When the plates are set you close the lids and place upside down in fridge.

###Sterilization protocol with pressure cooker
1. Sterilize all your surfaces with alcohol

- Fill pressure cooker with around 2-3 cm of water from the bottom.

- Turn the pot on and wait for it to boil

- When boiling turn down the heat just a bit

- Place glass wear in pot

- When boiling again start a timer for 20 minutes

- When done turn pot off and wait for it to depressurize.

- Take out glass wear and place upside down on paper towels





![](mdd-04.jpg)

_This was after the initial innoculation of the mycelium in the agar medium ._

![](mdd-05.jpg)
_This is the growth after the 3 weeks in the aquaponics setup ._ Aquaponics setup proved to be an ideal enviroment for the mycelium as it was above a 20 C and proved to be humid enough for the mycelium to grow .
## Palm Fibers
![](final-review.jpg)
_Palm fibers_

It started with desk research about understanding the properties of the palm fibers . This led me to finding this [ paper](https://airccse.com/msej/papers/2415msej01.pdf)

![](physicalproperties.png)

_This paper details the technical breakdown of palmfibers shown here are the physical properties of these fibers_
- Thus started the process of exploration in palm fibres . It for me started with exploration of the palm fibers . I did the big 3 test which were burning , boiling and hammering . After  a series of small experiments I could gauge the response of the material . I proceeded accordingly .

Following flowchart illustrates the response of the material and how I decided to proceed .

![](fiber-process-flow.png)
Explanation of the colour coding of the blocks

	- Red indicate that the works has been stopped in that particular direction
	- Green indicates that It showed promising results  and further experimentation was done on these samples with these samples being the base for them .
	- The yellow is the current process will be used to used to process the fibers.

1. *Burning* - Torching the fibers with a butane torch
didn't change the fibers structure and composition in any ways .Thus this method was abandoned .

2. *Hammering*- Hammering the material in its state didn't change its properties , thus this test was abandoned .

3. *Boiling*- Boiling the fiber for 4 hours. Led to the fibers being softened which led to being malleable and compliant.
This was base samples which were used for further testing.
  - *Hammering* => Hammering after Boiling separated the fibers from the lignin.
  This is the sample that I chose to move forward with.
  - *Airdrying* => Airdrying the boiled fibers on their own were left to air dry . The fibers then reconstituted to their original state .
  Thus this path of exploration was abandoned.
  - *Rehydration* => Keeping the fibers submerged in water keeps the fibers soft and malleable. This will be very helpful when innoculating with mycelium.
  - *Pulling apart by ha nd* => Process using hands to pull apart . This process yeilds long fibers which can be used to create strong substrate .



###Insights in the material :
The lignin binds the palm fibres . After boiling and hammering provides the best response for the material to be used .
Based on this testing I came up with a way of moving forward which we will be using the basic composition of the fibres and also keep its ductility .
So out of the all the test boiling and then hammering are deemed to be the most useful .

![](mdd-06.jpg)
_This is the palm fibers after boiling  and hammering . In the photo it illustrates the lignin degradation ._

![](fiberstructure.jpg)
_This is the fiberstructure which can be seen after the material has been soaked in ethanol for over 24 hours_
Image taken from [Jessica's Documentation](https://mdef.gitlab.io/jessica.guy/reflections/material-driven-design/)
- This is the current state of the oyster mycelium . I will shortly be doing the transfer from the petri dish to the palm fibres .

- According to Jonathans suggestion I could use a solution of molasses to act as a fertilizer to mycelium spores .
- I am going to grow the mycelium in its final form . This is the Mould design for the final form . I will shortly be manufacturing the Mould.

- I also observed that there is a lot of  moisture build-up in the petri dish . And the palm fibres are not the best at retaining the moisture so periodic spraying of water is needed
This is the present state of the work which has been done .   


- My final use of this would be used to create mycelium foam used as packaging for my project which will be its final form .
This is the final mould design .(as of right now )

![](myco-foam.jpg)
_The green part of the fill be the substrate that I'm working on_

![](mold-design-start.png)
_This is the start of the mold design for the final form_
