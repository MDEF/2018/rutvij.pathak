#Atlas of Weak Signals

![](FullAtlas.jpg)
_This is the full atlas of what we worked in the course._

- This was the lecture at the start of Jose Luis Vicente's Lecture.
This is a breakdown of the Lectures which set the tone for this course which set the general direction for exploration in the each lecture.
The course was an identification of "Weak Signals" - The agreed upon definition was a trend before it becomes a trend. Following were 25 main weak signals which were explored collectively.

##25 weak signals that we have worked on:
- **A.  Surveillance Capitalism** -It was about current ecosystem of the social media and networks works as is engineered and incentivized. Points explored in this branch were -_
  - Attention protection
  - Dismantling filter bubbles
  - Circular data economy
  - The truth wars (Fake news)
  - Redesigning social media
  - Manipulation
- **B.  The Anthropocene** -
_It is defined as "The Anthropocene is a proposed epoch dating from the commencement of significant human impact on the Earth's geology and ecosystems, including, but not limited to, anthropogenic climate change." - wikipedia
Points explored in this branch were -_

  - Longtermism
  - Fighting Anthropocene conflicts
  - Carbon neutral lifestyles
  - Interspecies Collaboration
  - Climate Consciousness


- **C.  Future of Jobs** -
_We talked about the job market post-AI . exploration of what it means to have a "job" .Points explored in this branch were -_
  - Future Jobs
  - Human-machine creative collaborations
  - Fighting AI Bias
  - Tech for equality
  - Making UBI work


- **D.  The End of the Nation State** -
_We talked about future of the nations as a whole and also in the context of the current and possible crisis._

  - Making world governance
  - Rural futures
  - Pick your own passport
  - Refugee tech
  - Welfare State 2.0

- **E.  Exploring identity** -
_We talked about the post-gender world where the government needs to put in rules and regulations for this new change._

  - Non heteropatriarchal innovation
  - Imagining Futures that are non western centric
  - Reconfigure your body
  - Gender Fluidity
  - Disrupting ageism     




Coming to the intervention for my  project


![](Intervention1.jpg)
_The 3 main weak signals which are "End of Nation State" and "Tech for Equality " and "Future of Jobs"_

- The reason these signals have been chosen is explained later .

![](Intervention2.jpg)
_This is the inter-section at which my intervention exists_



- This is the intervention  is at the cross-roads of these points in the overall scope of this system.
This intervention is at the cross-roads of Rural futures , Refugee Technology , Tech for equality , Circular Data Economy and Human Machine Collaboration

- **Refugee Tech** - Exploring the context of this would be  it will include both climate refugee and people displaced with instable situations will be in need of heathcare . The reason being there will be dynamic range of people in a particular area thus leading to overloading of hospitals in that particular area .
Rural Futures - Traditional Heathcare has typically ignored the problems which are related in the rural areas such as lack of infrastructure and adverse conditions  for them to operate in.

- **Tech for Equality**  - This intervention would be increasing the equality of the access of healthcare in far -off places.

- **Circular Data Economy** - The data that is generated will be communally held will be held in  and generate intelligence for predictive analysis etc.

- **Human Machine Collaboration** -  In the context of this intervention human-machine intervention would be between device - patients and devices - healthcare workers.


Thus my intervention sits at the cross-roads between making devices and making sure they are
Received by the people who need them the most .

The course helped me understand the complexities of various factors  which are involved in designing an intervention in this space . It also used to explore the unexplored dimensions of the project .
The direction of the Refugee Tech I had not considered which is one of the most important issues of our times .  
