
#Design for  2050


- Designing for  2050 was about speculating the future in 2050 .  This took the way of the making Black Mirror-esque TV series.

- The concept of these TV series was heterotopia's . The fictional places where 2 can be 2 instances how the same place can have 2 meanings. This concept of heterotopia was to be explored in this tv series. Everyone was divided into 5 group each divided into 5 group who will produce each episode each.  Five cities were chosen from 5 continents . We chose Laos in Nigeria , as this city is supposed to experience highest levels of increase in sealevels submerging a large part of the city.

- We were inspired by the [Green Lake]([https://www.dailymail.co.uk/news/article-2414013/Green-Lake-Tragoess-Austria-The-park-disappears-water-year.html](https://www.dailymail.co.uk/news/article-2414013/Green-Lake-Tragoess-Austria-The-park-disappears-water-year.html)) in Tragoess, Austria  where once the snow melts the garden surrounding the lake is submerged in the water.

- Parts of cities will be flooded every night. These areas inherently be industrial in nature or mostly commercial in nature . City and buildings are floating. Artificial Mountains built around the city. Flooded areas are to avoid for people living in the mountains. Mountains become the elite areas of Laos.

## The story of the episode is written below :

# Prismatic Minds.

- Theme: ID

- Object: Totem & Senses

- Conflict: Hyperreality vs. Environment

**Our title: Floating Point**

**City: Lagos - Nigeria**

**Main Character: Amadia** (the lightning spirit) - 31 years old. middle class. educated biologist. Works at „Mycocapsel Inc.“ Lives on a mountain.

Once she has to go to the floating areas of the Transients (people living there - like the invisible). Siren sound signals that she has to go back before the area is flooded. Can’t go back because her ID is not working to cross the bridge. She has to stay in this area. ITs the first time that she sees how this area is flooded and how the people live during the evening/night. She comes in contact with the smell, physical objects, cooking and realises how unattached she is from her cultural Identity. She wants to conserve the smell so tries to catch in a can (totem) and bring it home on the next day.

1. It is a sunny morning of May 2050, in Lagos, Nigeria. Climate change impacted daily flooding periods. The city has a population of more than 32 million. So, adaptations on the infrastructure were needed. Floating areas were created, which needed to adapt 1 meter of water level rise. Also, artificial mountains were built, where the more privileged and wealthy people live. The flooded areas are avoided by them. They do not know about the dynamics that happen there during the night. During the day, some interactions still occur, and Amadia, a tech biologist, had some business to do in one of these floating areas that day...
2. Amadia is a Tech Biologist, 31 years old, from the middle class. She works in a Myco-capsule company that trains mushrooms to clean the earth contamination. Remediating toxins from human pollution.
3. Amadia went to Lagos floating area, with a size of 4.3 square kilometers, because she needed to pick up some samples of a special toxic present in these lands.
4. To get in the floating area she needs to cross a “Double Bridge”. Where she shows her ID card and then automatically the floors move her to the other side.
5. After collecting the samples she needs, she is ready to go back to the mountain. She heads to the bridge. But after passing her ID, the bridge isn’t moving. She gets nervous, but there is no one around to help her. It is only a machine. And it is getting dark…
6. She realizes that she will be not able to cross and starts to feel unsafe there. She is walking around desperately. Suddenly, in the middle of the darkness, she sees a small light. She gets attracted by that, walking towards a weird construction.
7. She sees some people, that were not the typical she knows. You wouldn’t see in the place there, during the evening or night. They were getting organized to start one activity.
8. She observes that they were setting up a space, a temporary space. People were making objects, cooking - activities that Amadia never saw before. In her society, everything is made digitally. People never manipulated or changed the shape of things manually. Handcraft was never seen.
9. She gets fascinated by this and realizes how much she lost connection to a cultural identity from her own city. The smell, the feeling, the social gathering…
10. Enchanted by the smell of the Ogbono soup, she finds a little can on the floor and try to capture that smell to take it home.
11. She spends the night with that community. When the sun starts to rise and it is time to go back to the mountain, she realises that the can, her totem, is not going to last… Maybe she will come back later that night.

The video :


[![Prismatic Minds](PrismaticMinds_FloatingPoint.png)](https://www.youtube.com/watch?v=_SvON89JxwM&feature=youtu.be)
