#03.

##  Designing for the real digital world - Ingi Freyr

##LOG OF THE WEEK  
- OCT 15 :  Presentation about the origin of the concept i.e. from the madeagain challenge of space 10.Ingi talked about the Distributed Design Market Platform . "
- OCT 16:  brain storming "

- OCT 17:  Designing
- OCT 18:  Designing + Manufacturing "
- OCT 19:  Manufacturing and Presentation "

## Personal Notes

"This week was the week of making furniture out of components which were thrown out on the street . The class discussed the various designs and proposals which were presented various common themes resonated with the whole class They are listed in no particular order  
- Chill Space - a space to relax and unwind . "
- Makerspace  - a place to work / operate the machinery during FabAcademy.
- Light Air - the problem of lack of lighting and natural air circulation needs to be addressed .
- Plants - i.e. the addition of greenery inside the studio space.
- Space distribution  - The modular arrangement of the space was discussed .  

"After discussion -  A call was made to have a common floor plan . and various projects were assigned to various groups ."

"Our group was assigned a to make the Fab-table which was to be used .during mostly for soldering , cutting and miscellaneous activities ."
"The height of the table was set to 95 cm which constitutes the standing height for the table . The table was to be made using wood and pinewood."

"Following is the image of the table which was designed in fusion 360 and assembled ."  

![](w3-fabtable-fusion.png)

"A test joint was fabricated to test the joinery methods for the legs of the table . "
![](w3-fabtable-fusion2.png)

![](w3-fabtable-finished.jpg)

"This is the partially finished fablab table "
