#05

##  Navigating Uncertainity

##LOG OF THE WEEK  s
- OCT 29 :  Lecture from Jose Luise de Vicente  " test
- OCT 30 :  Lecture from Pau Alsina "
- OCT 31 :  Lecture from Mariana."
- OCT 5  :  week wrap-up  "


## Personal Notes

1. Jose Vicente Luis .

-	He talked about today in the context of the planetary crisis in terms on environmental impact because of humans and its activities. He talked about the temporal scale i.e the time scale in which the evolution of Society to evolution of habits / rituals in modern Society. He talked about the group of antroposene group  i.e the group of geologists that was formed that started to keep track after the detonation of the atomic bomb that messed up the standard by which the geologists dated the rocks and samples . He also talked about the great acceleration i.e. acceleration of various factors such nitrous oxide , surface temperature of the oceans along increase in the human population growth.
- He also talked about various movements within environmentalism that are decresitionism , capitalist , Trauma , empathy .

Decresitionism - The key tenant of their thought being "we need to reduce the consumption and waste produced by our current lifestyle."
capitalists   -  The faction that's treating the problem of global warming as a purely technical problem with "countermeasures needing to be put in place to mitigate its effects and going on the same trajectory."
He briefly mentioned a group SUPERFLUX (http://superflux.in/#) .
He also taked about the concept of HYPEROBJECT.

2. PAU Alsina .

- Pau talked the place of community and the place in the Society. He discussed about ideas and discourses . He talked about giving animals certain rights and also rights to nature i.e. forests etc.
One of the most interesting things of this week has been the use of micro-technologies such as the chair and correct "bodily posture " thus checking the change .The very act of correcting somebody's posture exerts power on that individual. That is the something I want to explore and work on .
" Nobody could see but everybody could be subjected to "    
The interesting thing was that various private actions that cumulatively turn into political actions .

A comment that i would like to make on the path that humanity has taken is the that we're so far up the pyramid of the animal kingdom that we've forgotten the animal part of our existence the more "primordial" drives of our existence . This is somewhat illustrated in the eye of providance. The referencing of this image was said by a certain philosopher that I cannot recollect right now .

![](w5-eyeofprovidence.jpg)

3. Mariana Quintero

-Mariana discussed about the read-write technologies which was extremely interesting.


Reflection :
- Though this week was interesting I could not relate it to my projects .
